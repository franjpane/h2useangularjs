**Création de l’application Angular2 via Angular CLI**
Installons tout d’abord Angular CLI globalement sur notre machine. Cet outil va nous servir à générer la structure de notre application via une simple commande et à recompiler à la volée nos modifications :

`npm install -g @angular/cli`

**Créons ensuite notre application :**

`ng new api-lab --style=scss`

**Puis lançons notre serveur interne :**

`cd api-lab && ng serve --open`

Maintenant que notre application est lancée, vous pouvez vous rendre sur l’url indiquée dans votre console pour accéder à votre application :

> http://localhost:4200 

**Vous devriez alors voir apparaître : app works!**


# H2useangularjs

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
